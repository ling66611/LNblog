# LNblog

#### 介绍
个人博客管理系统由SSM三大框架结合前端bootstrap、easy UI、lay UI框架构建而成。 
可以完成对博客信息的修改、查询、添加、删除（写博客、注册新博客用户、博客类别、博主信息、友情链接信息）、评论审核（对博客发表评论/审核）、登录校验、登录拦截、修改密码、上传头像、刷新系统缓存、退出等功能。
